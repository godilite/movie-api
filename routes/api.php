<?php

use Illuminate\Http\Request;

Route::get('movies', 'API\MoviesController@Movies');
Route::get('characters', 'API\MoviesController@Characters');
