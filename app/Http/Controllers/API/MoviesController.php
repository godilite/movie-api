<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
class MoviesController extends Controller
{
    public $apiToken = '2Mm8adzZp5SdqEMXDdAQ';

    public function Movies(Request $request)
    {
        //get sort params
        $filter = $request->sortBy;
        //Setup Http Client
        $client = new Client(['base_uri' => 'https://the-one-api.herokuapp.com/v1/', 'headers' => ['Authorization' => 'Bearer '.$this->apiToken]]);
        $response = $client->request('GET', 'movie');
        $results = $response->getBody();
        $movies = json_decode($results, true);
        $size = count($movies['docs']);
        //convert currency
        for($i = 0; $i < $size; $i++)
        {
            foreach($movies['docs'] as $key=>$value) {
                // 1 dollar = 350 naira
                // 1 Million dollars  = 350,000,000 naira
                $budget =  (int)$value['budgetInMillions']*350000000;
                $movies['docs'][$key]['budgetInNaira'] = number_format(floatval($budget), 2);

               //Similar conversion is done to revenue

               $boxOffice =  (int)$value['boxOfficeRevenueInMillions']*350000000;
               $movies['docs'][$key]['boxOfficeRevenueInNaira'] = number_format(floatval($boxOffice), 2);
            }
        }
        //remove old keys "budget and revenue"
        foreach($movies['docs'] as $key=>$value) {
            unset($movies['docs'][$key]['budgetInMillions']);
            unset($movies['docs'][$key]['boxOfficeRevenueInMillions']);
        }
        if( $filter == 'boxOfficeRevenueInNaira' || $filter == 'runtimeInMinutes'|| $filter == 'budgetInNaira'){
            usort($movies['docs'], function($a, $b) use ($filter) {
                if ($filter == 'boxOfficeRevenueInNaira') {
                    return ($a[$filter] < $b[$filter]) ? 1 : -1;
                }
                return ($a[$filter] > $b[$filter]) ? 1 : -1;
            });
        }
        return $movies;
    }

    public function Characters(Request $request)
    {

        $sortBy = $request->sortBy;

        //Setup client
        $client = new Client(['base_uri' => 'https://the-one-api.herokuapp.com/v1/', 'headers' => ['Authorization' => 'Bearer '.$this->apiToken]]);
        $response = $client->request('GET', 'character');
        $results = $response->getBody();
        $resultsJson = json_decode($results, true);
        $characters = $resultsJson['docs'];
        if( $sortBy == 'race' || $sortBy == 'gender'){
            usort($characters, function($a, $b) use ($sortBy) {
                if ($sortBy == 'race') {
                    return ($a[$sortBy] > $b[$sortBy]) ? 1 : -1;
                }

                if ($sortBy == 'descending') {
                  array_multisort($characters, SORT_ASC);
                }

                if ($sortBy == 'ascending') {
                    array_multisort($characters, SORT_ASC);
                  }
                return ($a[$sortBy] > $b[$sortBy]) ? 1 : -1;
            });

        }
        if ($request->perPage == '') {
           $perPage = 100;
        }else{
         $perPage = $request->perPage;
        }
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentItems = array_slice($characters, $perPage * ($currentPage - 1), $perPage);

        $paginator = new LengthAwarePaginator($currentItems, count($characters), $perPage, $currentPage);
        return $paginator;

    }
}
