## API Documentation

Two api endpoints were created for the code task as follows
- https://arcane-inlet-87193.herokuapp.com/api/movies
- https://arcane-inlet-87193.herokuapp.com/api/characters

the movies endpoint takes the following parameters

- Method 'GET' 
- endpoint: '/movies'
- params: 'sortBy' => to sort, use any of the following values:                           'boxOfficeRevenueInNaira'  
                'runtimeInMinutes' 
                'budgetInNaira'

the characters API takes the followinf params:
- Method: 'GET'
- endpoint: '/characters'
- params: 'perPage' (takes any interger to determine the number of items              per page)
        'sortBy' => to sort, pass in any of the following strings:
        'race'
        'descending'
        'ascending'
